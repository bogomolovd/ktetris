plugins {
    kotlin("jvm") version "1.3.61" apply false
}

allprojects {
    group = "com.bogomolovd.ktetris"
    version = 1.0
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")

    repositories {
        jcenter()
    }

    val implementation by configurations
    val testImplementation by configurations

    dependencies {
        implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
        testImplementation("org.jetbrains.kotlin:kotlin-test")
        testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
    }
}