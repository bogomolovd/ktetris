package com.bogomolovd.ktetris.lib.shapes

import kotlin.test.Test
import kotlin.test.assertEquals

class ShapeTest {
    @Test
    fun `move shape to the left`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.moveLeft()

        assertEquals(Shape(getLShapePoints(basePoint.moveLeft())), lShape)
    }

    private fun getLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveUp(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveRight()
    )

    @Test
    fun `move shape to the right`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.moveRight()

        assertEquals(Shape(getLShapePoints(basePoint.moveRight())), lShape)
    }

    @Test
    fun `move shape down`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.moveDown()

        assertEquals(Shape(getLShapePoints(basePoint.moveDown())), lShape)
    }

    @Test
    fun `move shape up`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.moveUp()

        assertEquals(Shape(getLShapePoints(basePoint.moveUp())), lShape)
    }

    @Test
    fun `rotate shape right`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.rotateRight()

        assertEquals(Shape(getRotatedRightLShapePoints(basePoint)), lShape)
    }

    private fun getRotatedRightLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveRight(), basePoint, basePoint.moveLeft(), basePoint.moveLeft().moveDown()
    )

    @Test
    fun `rotate shape left`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.rotateLeft()

        assertEquals(Shape(getRotatedLeftLShapePoints(basePoint)), lShape)
    }

    private fun getRotatedLeftLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveLeft(), basePoint, basePoint.moveRight(), basePoint.moveRight().moveUp()
    )
}