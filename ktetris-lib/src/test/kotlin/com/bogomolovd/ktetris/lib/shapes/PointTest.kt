package com.bogomolovd.ktetris.lib.shapes

import kotlin.test.Test
import kotlin.test.assertEquals

class PointTest {
    @Test
    fun `move point to the left`() {
        val point = Point(1, 1)
        assertEquals(Point(0, 1), point.moveLeft())
    }

    @Test
    fun `move point to the right`() {
        val point = Point(1, 1)
        assertEquals(Point(2, 1), point.moveRight())
    }

    @Test
    fun `move point down`() {
        val point = Point(1, 1)
        assertEquals(Point(1, 2), point.moveDown())
    }

    @Test
    fun `move point up`() {
        val point = Point(1, 1)
        assertEquals(Point(1, 0), point.moveUp())
    }

    @Test
    fun `rotate point right`() {
        val basePoint = Point(1, 1)
        val point = Point(1, 2)
        assertEquals(Point(0, 1), point.rotateRight(basePoint))
    }

    @Test
    fun `rotate point left`() {
        val basePoint = Point(1, 1)
        val point = Point(1, 2)
        assertEquals(Point(2, 1), point.rotateLeft(basePoint))
    }
}