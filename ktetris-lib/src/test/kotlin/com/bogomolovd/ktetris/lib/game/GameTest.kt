package com.bogomolovd.ktetris.lib.game

import com.bogomolovd.ktetris.lib.shapes.Point
import com.bogomolovd.ktetris.lib.shapes.Shape
import org.junit.Test
import kotlin.test.assertEquals

class GameTest {
    private val gameFieldSize = GameFieldSize(10, 10)

    @Test
    fun `move falling shape to the left`() {
        val basePoint = Point(1, 1)
        val game = initializeGame(basePoint)

        game.moveFallingShapeLeft()

        assertEquals(Shape(getLShapePoints(basePoint.moveLeft())), game.fallingShape)
    }

    private fun initializeGame(basePoint: Point): Game {
        val game = Game(gameFieldSize)
        val lShape = Shape(getLShapePoints(basePoint))
        game.fallingShape = lShape
        return game
    }

    private fun getLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveUp(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveRight()
    )

    @Test
    fun `move falling shape to the left doesn't move shape out of bounds`() {
        val basePoint = Point(0, 1)
        val game = initializeGame(basePoint)

        game.moveFallingShapeLeft()

        assertEquals(Shape(getLShapePoints(basePoint)), game.fallingShape)
    }

    @Test
    fun `move falling shape to the left doesn't move shape into placed points`() {
        val basePoint = Point(2, 1)
        val game = initializeGame(basePoint)
        placeShapeOnGameField(Shape(getLShapePoints(basePoint.moveLeft().moveLeft())), game.gameField)

        game.moveFallingShapeLeft()

        assertEquals(Shape(getLShapePoints(basePoint)), game.fallingShape)
    }

    private fun placeShapeOnGameField(shape: Shape, gameField: Array<Array<Boolean>>) =
        shape.points.forEach { gameField[it.y][it.x] = true }

    @Test
    fun `move falling shape to the right`() {
        val basePoint = Point(1, 1)
        val game = initializeGame(basePoint)

        game.moveFallingShapeRight()

        assertEquals(Shape(getLShapePoints(basePoint.moveRight())), game.fallingShape)
    }

    @Test
    fun `move falling shape to the right doesn't move shape out of bounds`() {
        val basePoint = Point(gameFieldSize.width - 2, 1)
        val game = initializeGame(basePoint)

        game.moveFallingShapeRight()

        assertEquals(Shape(getLShapePoints(basePoint)), game.fallingShape)
    }

    @Test
    fun `move falling shape to the right doesn't move shape into placed points`() {
        val basePoint = Point(gameFieldSize.width - 4, 1)
        val game = initializeGame(basePoint)
        placeShapeOnGameField(Shape(getLShapePoints(basePoint.moveRight().moveRight())), game.gameField)

        game.moveFallingShapeRight()

        assertEquals(Shape(getLShapePoints(basePoint)), game.fallingShape)
    }

    @Test
    fun `move falling shape down`() {
        val basePoint = Point(1, 1)
        val game = initializeGame(basePoint)

        game.moveFallingShapeDown()

        assertEquals(Shape(getLShapePoints(Point(1, gameFieldSize.height - 2))), game.fallingShape)
    }

    @Test
    fun `move falling shape to the down doesn't move shape into placed points`() {
        val basePoint = Point(1, 1)
        val game = initializeGame(basePoint)
        placeShapeOnGameField(Shape(getLShapePoints(Point(1, gameFieldSize.height - 2))), game.gameField)

        game.moveFallingShapeDown()

        assertEquals(Shape(getLShapePoints(Point(1, gameFieldSize.height - 5))), game.fallingShape)
    }

    @Test
    fun `rotate falling shape right`() {
        val basePoint = Point(1, 1)
        val lShape = Shape(getLShapePoints(basePoint))

        lShape.rotateRight()

        assertEquals(Shape(getRotatedRightLShapePoints(basePoint)), lShape)
    }

    private fun getRotatedRightLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveRight(), basePoint, basePoint.moveLeft(), basePoint.moveLeft().moveDown()
    )

    @Test
    fun `rotate falling shape to the right`() {
        val basePoint = Point(1, 1)
        val game = initializeGame(basePoint)

        game.rotateFallingShapeRight()

        assertEquals(Shape(getRotatedRightLShapePoints(basePoint)), game.fallingShape)
    }

    @Test
    fun `rotate falling shape to the right doesn't move shape out of bounds`() {
        val basePoint = Point(0, 1)
        val game = initializeGame(basePoint)

        game.rotateFallingShapeRight()

        assertEquals(Shape(getLShapePoints(basePoint)), game.fallingShape)
    }

    @Test
    fun `rotate falling shape to the right doesn't move shape into placed points`() {
        val basePoint = Point(1, 2)
        val game = initializeGame(basePoint)
        placeShapeOnGameField(Shape(getLShapePoints(basePoint.moveRight().moveUp())), game.gameField)

        game.rotateFallingShapeRight()

        assertEquals(Shape(getLShapePoints(basePoint)), game.fallingShape)
    }

    @Test
    fun `game is over if there is no place to spawn falling shape`() {
        val basePoint = Point(gameFieldSize.width / 2, 1)
        val game = initializeGame(basePoint)
        placeShapeOnGameField(Shape(getLShapePoints(basePoint.moveDown().moveDown().moveDown())), game.gameField)

        game.loop()

        assert(game.gameOver)
    }

    @Test
    fun `rows are cleared when filled and score increased`() {
        val basePoint = Point(0, 1)
        val game = initializeGame(basePoint)
        fillLastRow(game.gameField)
        placeShapeOnGameField(Shape(getLShapePoints(Point(2, gameFieldSize.height - 3))), game.gameField)
        placeShapeOnGameField(Shape(getLShapePoints(Point(4, gameFieldSize.height - 3))), game.gameField)
        placeShapeOnGameField(Shape(getLShapePoints(Point(6, gameFieldSize.height - 3))), game.gameField)
        placeShapeOnGameField(Shape(getLShapePoints(Point(8, gameFieldSize.height - 3))), game.gameField)

        game.moveFallingShapeDown()
        game.loop()

        assert(rowIsEmpty(game.gameField, gameFieldSize.height - 4))
        assert(rowIsEmpty(game.gameField, gameFieldSize.height - 3))
        assert(onlyEvenPointsFilledOnRow(game.gameField, gameFieldSize.height - 2))
        assert(onlyEvenPointsFilledOnRow(game.gameField, gameFieldSize.height - 1))
        assertEquals(game.score, 3)
    }

    private fun fillLastRow(gameField: Array<Array<Boolean>>) =
        gameField[gameFieldSize.height - 1].indices.forEach {
            gameField[gameFieldSize.height - 1][it] = true
        }

    private fun onlyEvenPointsFilledOnRow(gameField: Array<Array<Boolean>>, rowIndex: Int) =
        gameField[rowIndex].indices.all {
            if (it % 2 == 0) gameField[rowIndex][it]
            else !gameField[rowIndex][it]
        }

    private fun rowIsEmpty(gameField: Array<Array<Boolean>>, rowIndex: Int) = gameField[rowIndex].none { it }
}