package com.bogomolovd.ktetris.lib.shapes

import org.junit.Test
import kotlin.test.assertEquals

class SquareTest {
    @Test
    fun `rotation does nothing`() {
        val basePoint = Point(1, 1)
        val square = Square(basePoint)

        square.rotateRight()
        assertEquals(Square(basePoint), square)

        square.rotateLeft()
        assertEquals(Square(basePoint), square)
    }
}