package com.bogomolovd.ktetris.lib.game

data class GameFieldSize(val width: Int, val height: Int)