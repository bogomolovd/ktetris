package com.bogomolovd.ktetris.lib.shapes

/*
    Point rotation implemented by rotation matrix https://en.wikipedia.org/wiki/Rotation_matrix.
    Rotation functions have a base point as an argument to take it as (0,0) point
     and normalize other points using it.
    val normalizedPoint = Point(x - base.x, y - base.y) = (0,1)

    After that rotation matrix formula is applied to X and Y coordinates of a point.
    val p = Point(
        normalizedPoint.x * cos(PI/2).toInt() - normalizedPoint.y * sin(PI/2).toInt(),
        normalizedPoint.x * sin(PI/2).toInt() + normalizedPoint.y * cos(PI/2).toInt()
    ) = (-1,0)

    And finally a point is returned back to its original coordinates relatively to base point.
    return Point(p.x + base.x, p.y + base.y) = (0,1)
*/

internal data class Point(val x: Int, val y: Int) {
    internal fun moveLeft() = Point(x - 1, y)

    internal fun moveRight() = Point(x + 1, y)

    internal fun moveDown() = Point(x, y + 1)

    internal fun moveUp() = Point(x, y - 1)

    internal fun rotateRight(base: Point) = Point(-y + base.y + base.x, x - base.x + base.y)

    internal fun rotateLeft(base: Point) = Point(y - base.y + base.x, -x + base.x + base.y)
}