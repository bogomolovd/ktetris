package com.bogomolovd.ktetris.lib.shapes

import kotlin.random.Random

internal object ShapeFactory {
    private val random = Random.Default

    internal fun getShape(gameFieldWidth: Int): Shape {
        val basePoint = Point(gameFieldWidth / 2, 0)
        val shape = getNextShape(basePoint)
        rotateShape(shape)
        moveShapeDownIfOutOfBounds(shape)
        return shape
    }

    private fun getNextShape(basePoint: Point): Shape =
        when (random.nextInt(7)) {
            0 -> Square(basePoint)
            1 -> Shape(getLinePoints(basePoint))
            2 -> Shape(getLShapePoints(basePoint))
            3 -> Shape(getInvertedLShapePoints(basePoint))
            4 -> Shape(getZShapePoints(basePoint))
            5 -> Shape(getInvertedZShapePoints(basePoint))
            else -> Shape(getPyramidPoints(basePoint))
        }

    private fun getLinePoints(basePoint: Point) = arrayListOf(
        basePoint.moveUp(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveDown()
    )

    private fun getLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveUp(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveRight()
    )

    private fun getInvertedLShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveUp(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveLeft()
    )

    private fun getZShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveLeft(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveRight()
    )

    private fun getInvertedZShapePoints(basePoint: Point) = arrayListOf(
        basePoint.moveRight(), basePoint, basePoint.moveDown(), basePoint.moveDown().moveLeft()
    )

    private fun getPyramidPoints(basePoint: Point) = arrayListOf(
        basePoint.moveLeft(), basePoint, basePoint.moveRight(), basePoint.moveUp()
    )

    private fun rotateShape(shape: Shape) {
        val rotation = random.nextInt(4)
        (0..rotation).forEach { _ -> shape.rotateRight() }
    }

    private fun moveShapeDownIfOutOfBounds(shape: Shape) {
        while (shape.points.any { point -> point.y < 0 }) shape.moveDown()
    }
}