package com.bogomolovd.ktetris.lib.shapes

internal const val SHAPE_MAX_SIZE = 4

internal open class Shape(points: List<Point>) {
    var points: List<Point> = points
        private set

    internal open fun rotateRight() {
        points = points.map { it.rotateRight(points[1]) }
    }

    internal open fun rotateLeft() {
        points = points.map { it.rotateLeft(points[1]) }
    }

    internal fun moveLeft() {
        points = points.map(Point::moveLeft)
    }

    internal fun moveRight() {
        points = points.map(Point::moveRight)
    }

    internal fun moveDown() {
        points = points.map(Point::moveDown)
    }

    internal fun moveUp() {
        points = points.map(Point::moveUp)
    }

    override fun toString(): String {
        return "${javaClass.simpleName}(points=$points)"
    }

    override fun equals(other: Any?): Boolean =
        this === other || (other is Shape && points == other.points)

    override fun hashCode(): Int = points.hashCode()
}