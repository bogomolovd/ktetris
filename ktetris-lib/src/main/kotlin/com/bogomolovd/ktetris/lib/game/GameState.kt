package com.bogomolovd.ktetris.lib.game

import com.bogomolovd.ktetris.lib.shapes.Point
import com.bogomolovd.ktetris.lib.shapes.SHAPE_MAX_SIZE
import com.bogomolovd.ktetris.lib.shapes.Shape

data class GameState(
    val gameOver: Boolean, val score: Int,
    val nextShapePoints: Array<Array<Boolean>>,
    val gameField: Array<Array<Boolean>>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameState

        if (gameOver != other.gameOver) return false
        if (score != other.score) return false
        if (!gameField.contentDeepEquals(other.gameField)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = gameOver.hashCode()
        result = 31 * result + score
        result = 31 * result + gameField.contentDeepHashCode()
        return result
    }

    internal companion object {
        internal fun buildGameState(game: Game): GameState {
            val gameFieldWithFallingShape =
                placeFallingShapeOnClonedGameField(game.gameField, game.fallingShape)
            val nextShapePoints = convertNextShapeToArrayOfPoints(game.nextShape)
            return GameState(game.gameOver, game.score, nextShapePoints, gameFieldWithFallingShape)
        }

        private fun placeFallingShapeOnClonedGameField(
            gameField: Array<Array<Boolean>>, fallingShape: Shape
        ): Array<Array<Boolean>> {
            val gameFieldClone = gameField.map { it.clone() }.toTypedArray()
            fallingShape.points.forEach { gameFieldClone[it.y][it.x] = true }
            return gameFieldClone
        }

        private fun convertNextShapeToArrayOfPoints(nextShape: Shape): Array<Array<Boolean>> {
            val nextShapePoints = Array(SHAPE_MAX_SIZE) { Array(SHAPE_MAX_SIZE) { false } }
            val minX = nextShape.points.minBy(Point::x)!!.x
            nextShape.points.forEach { nextShapePoints[it.y][it.x - minX] = true }
            return nextShapePoints
        }
    }
}