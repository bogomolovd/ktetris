package com.bogomolovd.ktetris.lib.game

import com.bogomolovd.ktetris.lib.game.GameState.Companion.buildGameState
import com.bogomolovd.ktetris.lib.shapes.Point
import com.bogomolovd.ktetris.lib.shapes.Shape
import com.bogomolovd.ktetris.lib.shapes.ShapeFactory

class Game(private val gameFieldSize: GameFieldSize) {
    private val gameStateEmitter = GameStateEmitter(this)
    internal var score = 0
    internal var gameField: Array<Array<Boolean>> = Array(gameFieldSize.height) {
        Array(gameFieldSize.width) { false }
    }
    internal var fallingShape: Shape = ShapeFactory.getShape(gameFieldSize.width)
    internal var nextShape: Shape = ShapeFactory.getShape(gameFieldSize.width)
    internal var gameOver = false

    fun start() {
        if (gameOver) return
        gameStateEmitter.start()
    }

    fun stop() = gameStateEmitter.stop()

    fun subscribeOnNewGameState(subscriber: GameStateSubscriber) =
        gameStateEmitter.addGameStateSubscriber(subscriber)

    fun unsubscribeFromNewGameState(subscriber: GameStateSubscriber) =
        gameStateEmitter.removeGameStateSubscriber(subscriber)

    internal fun loop(): GameState {
        fallingShape.moveDown()
        if (fallingShapeOutOfBounds() || fallingShapeIntersectsFilledPoints()) {
            fallingShape.moveUp()
            placeFallingShape()
            val clearedRowsCount = clearFullRows()
            increaseScoreAndDifficultyIfNeeded(clearedRowsCount)
            makeNextShapeFalling()
            if (fallingShapeIntersectsFilledPoints()) {
                gameOver = true
            }
        }
        return buildGameState(this)
    }

    private fun fallingShapeIntersectsFilledPoints() = fallingShape.points.any { gameField[it.y][it.x] }

    private fun placeFallingShape() = fallingShape.points.forEach { gameField[it.y][it.x] = true }

    private fun fallingShapeOutOfBounds() = fallingShape.points.minBy(Point::x)!!.x < 0
            || fallingShape.points.maxBy(Point::x)!!.x > gameFieldSize.width - 1
            || fallingShape.points.minBy(Point::y)!!.y < 0
            || fallingShape.points.maxBy(Point::y)!!.y > gameFieldSize.height - 1

    private fun clearFullRows(): Int {
        val fullRowsIndexes =
            gameField.indices.filter { rowIndex -> gameField[rowIndex].all { it } }

        if (fullRowsIndexes.isEmpty()) return 0

        (gameFieldSize.height - 1 downTo 0).forEach { rowIndex ->
            if (rowIndex in fullRowsIndexes) {
                clearRow(rowIndex)
            } else {
                val clearedRowsBelowCount = fullRowsIndexes.filter { it > rowIndex }.count()
                if (clearedRowsBelowCount != 0 && !isRowEmpty(rowIndex)) {
                    shiftRowDown(rowIndex, clearedRowsBelowCount)
                    clearRow(rowIndex)
                }
            }
        }

        return fullRowsIndexes.size
    }

    private fun clearRow(row: Int) = (0 until gameFieldSize.width)
        .forEach { column -> gameField[row][column] = false }

    private fun shiftRowDown(row: Int, shift: Int) = (0 until gameFieldSize.width)
        .forEach { column -> gameField[row + shift][column] = gameField[row][column] }

    private fun isRowEmpty(row: Int) = gameField[row].all { !it }

    private fun increaseScoreAndDifficultyIfNeeded(clearedRowsCount: Int) {
        if (clearedRowsCount == 0) return
        val oldScore = score
        score += clearedRowsCount * (clearedRowsCount + 1) / 2
        if (score.div(10) != oldScore.div(10)) gameStateEmitter.reducePeriodBetweenEvents()
    }

    private fun makeNextShapeFalling() {
        fallingShape = nextShape
        nextShape = ShapeFactory.getShape(gameFieldSize.width)
    }

    fun moveFallingShapeLeft(): GameState {
        fallingShape.moveLeft()
        if (fallingShapeOutOfBounds() || fallingShapeIntersectsFilledPoints()) fallingShape.moveRight()
        return buildGameState(this)
    }

    fun moveFallingShapeRight(): GameState {
        fallingShape.moveRight()
        if (fallingShapeOutOfBounds() || fallingShapeIntersectsFilledPoints()) fallingShape.moveLeft()
        return buildGameState(this)
    }

    fun moveFallingShapeDown(): GameState {
        while (!fallingShapeOutOfBounds() && !fallingShapeIntersectsFilledPoints()) fallingShape.moveDown()
        fallingShape.moveUp()
        return buildGameState(this)
    }

    fun rotateFallingShapeRight(): GameState {
        fallingShape.rotateRight()
        if (fallingShapeOutOfBounds() || fallingShapeIntersectsFilledPoints()) fallingShape.rotateLeft()
        return buildGameState(this)
    }
}
