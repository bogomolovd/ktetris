package com.bogomolovd.ktetris.lib.shapes

internal class Square(basePoint: Point) : Shape(
    arrayListOf(
        basePoint.moveLeft(), basePoint,
        basePoint.moveLeft().moveDown(), basePoint.moveDown()
    )
) {
    override fun rotateRight() {
        return
    }

    override fun rotateLeft() {
        return
    }
}