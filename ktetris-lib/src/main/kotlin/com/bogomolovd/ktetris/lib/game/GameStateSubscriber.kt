package com.bogomolovd.ktetris.lib.game

interface GameStateSubscriber {
    fun onNewGameState(newGameState: GameState)
}