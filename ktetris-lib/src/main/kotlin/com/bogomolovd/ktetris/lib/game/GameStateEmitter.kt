package com.bogomolovd.ktetris.lib.game

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import kotlin.math.max

private const val INITIAL_PERIOD_MS: Long = 1000

internal class GameStateEmitter(private val game: Game) : Runnable {
    private val executorService =
        Executors.newSingleThreadScheduledExecutor() as ScheduledExecutorService
    private val gameStateSubscribers = arrayListOf<GameStateSubscriber>()
    private var future: ScheduledFuture<*>? = null
    private var currentPeriodMs: Long = INITIAL_PERIOD_MS

    internal fun start() {
        future = executorService.scheduleAtFixedRate(this, 0, currentPeriodMs, TimeUnit.MILLISECONDS)
    }

    internal fun stop() {
        future?.cancel(false)
        executorService.shutdownNow()
    }

    internal fun reducePeriodBetweenEvents() {
        future?.cancel(false)
        currentPeriodMs = max(currentPeriodMs - 50, 100)
        start()
    }

    internal fun addGameStateSubscriber(gameStateSubscriber: GameStateSubscriber) =
        gameStateSubscribers.add(gameStateSubscriber)

    internal fun removeGameStateSubscriber(gameStateSubscriber: GameStateSubscriber) =
        gameStateSubscribers.remove(gameStateSubscriber)

    override fun run() {
        val newGameState = game.loop()
        gameStateSubscribers.forEach { it.onNewGameState(newGameState) }
        if (newGameState.gameOver)
            stop()
    }
}