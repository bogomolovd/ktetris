plugins {
    application
}

dependencies {
    implementation(project(":ktetris-lib"))
    implementation("com.googlecode.lanterna:lanterna:3.0.2")
}

application {
    mainClassName = "com.bogomolovd.ktetris.cli.MainKt"
}

