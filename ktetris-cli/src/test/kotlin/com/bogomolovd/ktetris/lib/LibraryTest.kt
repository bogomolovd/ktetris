package com.bogomolovd.ktetris.lib

import kotlin.test.Test
import kotlin.test.assertNotNull

class LibraryTest {
    @Test
    fun testSomeLibraryMethod() {
        assertNotNull("hello", "someLibraryMethod should return 'true'")
    }
}
