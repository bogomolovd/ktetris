package com.bogomolovd.ktetris.cli

import com.googlecode.lanterna.input.KeyStroke
import java.util.concurrent.Executors

class InputListener(
    private val readerFunction: () -> KeyStroke,
    private val handlerFunction: (KeyStroke) -> Unit?
) : Runnable {
    private val inputHandlerThreadExecutor = Executors.newSingleThreadExecutor()

    init {
        inputHandlerThreadExecutor.submit(this)
    }

    fun stop() {
        inputHandlerThreadExecutor.shutdownNow()
    }

    override fun run() {
        handlerFunction(readerFunction())
        inputHandlerThreadExecutor.submit(this)
    }
}