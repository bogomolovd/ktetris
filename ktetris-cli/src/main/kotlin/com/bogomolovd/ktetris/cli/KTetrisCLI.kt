package com.bogomolovd.ktetris.cli

import com.bogomolovd.ktetris.lib.game.Game
import com.bogomolovd.ktetris.lib.game.GameFieldSize
import com.bogomolovd.ktetris.lib.game.GameState
import com.bogomolovd.ktetris.lib.game.GameStateSubscriber
import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType
import com.googlecode.lanterna.terminal.DefaultTerminalFactory
import com.googlecode.lanterna.terminal.Terminal
import java.util.concurrent.ArrayBlockingQueue

class KTetrisCLI(gameFieldSize: GameFieldSize) : GameStateSubscriber {
    private val terminal: Terminal
    private val uiThreadTasksQueue = ArrayBlockingQueue<() -> Unit>(10)
    private val keyStrokeToGameStateOperationMapping = mapOf(
        Pair('a', Game::moveFallingShapeLeft),
        Pair('d', Game::moveFallingShapeRight),
        Pair('s', Game::moveFallingShapeDown),
        Pair(' ', Game::rotateFallingShapeRight)
    )
    private val inputListener: InputListener
    private var uiDrawer: UIDrawer
    private var game: Game = Game(gameFieldSize)
    private var gameOver: Boolean = false

    init {
        terminal = DefaultTerminalFactory()
            .setInitialTerminalSize(getTerminalSizeForGameFieldSize(gameFieldSize))
            .setTerminalEmulatorTitle(TERMINAL_TITLE)
            .createTerminal()!!
        terminal.setCursorVisible(false)
        inputListener = InputListener(terminal::readInput, this::handleKeyStroke)
        uiDrawer = UIDrawer(terminal, gameFieldSize)
    }

    private fun getTerminalSizeForGameFieldSize(gameFieldSize: GameFieldSize) =
        TerminalSize(
            gameFieldSize.width * GAME_STATE_SIZE_MULTIPLIER +
                    2 * UI_OFFSET_FROM_WINDOW_BOUNDARIES + RIGHT_SIDE_UI_HORIZONTAL_OFFSET +
                    RIGHT_SIDE_UI_WIDTH + HORIZONTAL_BOUNDARIES_WIDTH,
            gameFieldSize.height * GAME_STATE_SIZE_MULTIPLIER +
                    2 * UI_OFFSET_FROM_WINDOW_BOUNDARIES + HORIZONTAL_BOUNDARIES_WIDTH
        )

    fun start() {
        game.subscribeOnNewGameState(this)
        game.start()
        while (!gameOver) uiThreadTasksQueue.take().invoke()
        game.stop()
        game.unsubscribeFromNewGameState(this)
        inputListener.stop()
    }

    override fun onNewGameState(newGameState: GameState) {
        uiThreadTasksQueue.put {
            uiDrawer.drawUI(newGameState)
            if (newGameState.gameOver)
                gameOver = true
        }
    }

    private fun handleKeyStroke(keyStroke: KeyStroke) =
        if (keyStroke.keyType == KeyType.EOF) uiThreadTasksQueue.put { gameOver = true }
        else {
            keyStrokeToGameStateOperationMapping[keyStroke.character]?.let {
                val newGameState = it.invoke(game)
                onNewGameState(newGameState)
            }
        }
}