package com.bogomolovd.ktetris.cli

const val GAME_STATE_SIZE_MULTIPLIER = 2
const val UI_OFFSET_FROM_WINDOW_BOUNDARIES = 1
const val RIGHT_SIDE_UI_HORIZONTAL_OFFSET = 2
const val SCORE_UI_VERTICAL_OFFSET = 1
const val GAME_OVER_UI_VERTICAL_OFFSET = 3
const val NEXT_SHAPE_UI_VERTICAL_OFFSET = 5
const val RIGHT_SIDE_UI_WIDTH = 11
const val HORIZONTAL_BOUNDARIES_WIDTH = 2
const val TERMINAL_TITLE = "KTetris"
const val SCORE_LABEL = "Score:"
const val NEXT_SHAPE_LABEL = "Next shape:"
const val GAME_OVER_LABEL = "Game over!"
const val VERTICAL_BOUNDARY_CHAR = '║'
const val HORIZONTAL_BOUNDARY_CHAR = '═'
const val LEFT_TOP_CORNER_CHAR = '╔'
const val RIGHT_TOP_CORNER_CHAR = '╗'
const val LEFT_BOTTOM_CORNER_CHAR = '╚'
const val RIGHT_BOTTOM_CORNER_CHAR = '╝'
const val POINT_CHAR = '•'