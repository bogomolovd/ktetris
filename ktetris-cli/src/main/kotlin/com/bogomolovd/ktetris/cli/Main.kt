package com.bogomolovd.ktetris.cli

import com.bogomolovd.ktetris.lib.game.GameFieldSize

fun main() {
    val gameFieldSize = GameFieldSize(10, 15)
    val tetris = KTetrisCLI(gameFieldSize)
    tetris.start()
}

