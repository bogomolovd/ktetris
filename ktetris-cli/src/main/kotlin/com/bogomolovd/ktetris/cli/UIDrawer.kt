package com.bogomolovd.ktetris.cli

import com.bogomolovd.ktetris.lib.game.GameFieldSize
import com.bogomolovd.ktetris.lib.game.GameState
import com.googlecode.lanterna.terminal.Terminal

class UIDrawer(private val terminal: Terminal, gameFieldSize: GameFieldSize) {
    private val leftBoundaryXCoordinate = UI_OFFSET_FROM_WINDOW_BOUNDARIES
    private val rightBoundaryXCoordinate =
        gameFieldSize.width * GAME_STATE_SIZE_MULTIPLIER + 2 * UI_OFFSET_FROM_WINDOW_BOUNDARIES
    private val topBoundaryYCoordinate = UI_OFFSET_FROM_WINDOW_BOUNDARIES
    private val bottomBoundaryYCoordinate =
        gameFieldSize.height * GAME_STATE_SIZE_MULTIPLIER + 2 * UI_OFFSET_FROM_WINDOW_BOUNDARIES

    fun drawUI(gameState: GameState) {
        terminal.clearScreen()
        drawGameField(gameState)
        terminal.flush()
    }

    private fun drawGameField(gameState: GameState) {
        drawVerticalBoundaries()
        drawHorizontalBoundaries()
        drawFilledPoints(gameState.gameField)
        drawScore(gameState.score)
        drawNextShape(gameState.nextShapePoints)
        if (gameState.gameOver) {
            drawGameOver()
        }
    }

    private fun drawVerticalBoundaries() =
        (topBoundaryYCoordinate..bottomBoundaryYCoordinate).forEach { row ->
            putCharacterAtPosition(VERTICAL_BOUNDARY_CHAR, leftBoundaryXCoordinate, row)
            putCharacterAtPosition(VERTICAL_BOUNDARY_CHAR, rightBoundaryXCoordinate, row)
        }

    private fun drawHorizontalBoundaries() =
        (leftBoundaryXCoordinate..rightBoundaryXCoordinate).forEach { column ->
            putCharacterAtPosition(getCharForHorizontalBoundary(column, true), column, topBoundaryYCoordinate)
            putCharacterAtPosition(getCharForHorizontalBoundary(column, false), column, bottomBoundaryYCoordinate)
        }

    private fun getCharForHorizontalBoundary(column: Int, top: Boolean) = when (column) {
        leftBoundaryXCoordinate -> if (top) LEFT_TOP_CORNER_CHAR else LEFT_BOTTOM_CORNER_CHAR
        rightBoundaryXCoordinate -> if (top) RIGHT_TOP_CORNER_CHAR else RIGHT_BOTTOM_CORNER_CHAR
        else -> HORIZONTAL_BOUNDARY_CHAR
    }

    private fun drawFilledPoints(gameField: Array<Array<Boolean>>) =
        gameField.indices.forEach { row ->
            gameField[row].indices
                .filter { column -> gameField[row][column] }
                .forEach { column -> drawPointFromGameField(column, row) }
        }

    private fun drawPointFromGameField(x: Int, y: Int) {
        // had to add 1 in order to shift a point from horizontal and vertical boundaries
        val pointXCoordinate: Int = x * GAME_STATE_SIZE_MULTIPLIER + leftBoundaryXCoordinate + 1
        val pointYCoordinate: Int = y * GAME_STATE_SIZE_MULTIPLIER + topBoundaryYCoordinate + 1
        drawPoint(pointXCoordinate, pointYCoordinate)
    }

    private fun drawPoint(x: Int, y: Int) {
        putCharacterAtPosition(POINT_CHAR, x, y)
        putCharacterAtPosition(POINT_CHAR, x + 1, y)
        putCharacterAtPosition(POINT_CHAR, x, y + 1)
        putCharacterAtPosition(POINT_CHAR, x + 1, y + 1)
    }

    private fun drawScore(score: Int) {
        val scoreXCoordinate = rightBoundaryXCoordinate + RIGHT_SIDE_UI_HORIZONTAL_OFFSET
        val scoreYCoordinate = topBoundaryYCoordinate + SCORE_UI_VERTICAL_OFFSET
        terminal.setCursorPosition(scoreXCoordinate, scoreYCoordinate)
        SCORE_LABEL.forEach { char -> terminal.putCharacter(char) }
        // just below "Score:" label
        terminal.setCursorPosition(scoreXCoordinate, scoreYCoordinate + 1)
        score.toString().forEach { char -> terminal.putCharacter(char) }
    }

    private fun drawNextShape(nextShapePoints: Array<Array<Boolean>>) {
        val nextShapeLabelXCoordinate = rightBoundaryXCoordinate + RIGHT_SIDE_UI_HORIZONTAL_OFFSET
        val nextShapeLabelYCoordinate = topBoundaryYCoordinate + NEXT_SHAPE_UI_VERTICAL_OFFSET
        terminal.setCursorPosition(nextShapeLabelXCoordinate, nextShapeLabelYCoordinate)
        NEXT_SHAPE_LABEL.forEach { char -> terminal.putCharacter(char) }
        nextShapePoints.indices.forEach { row ->
            nextShapePoints[row].indices
                .filter { column -> nextShapePoints[row][column] }
                .forEach { column -> drawPointFromNextShape(column, row) }
        }
    }

    private fun drawPointFromNextShape(x: Int, y: Int) {
        val pointXCoordinate: Int = x * GAME_STATE_SIZE_MULTIPLIER +
                rightBoundaryXCoordinate + RIGHT_SIDE_UI_HORIZONTAL_OFFSET
        // just below next shape label
        val pointYCoordinate: Int = y * GAME_STATE_SIZE_MULTIPLIER +
                topBoundaryYCoordinate + NEXT_SHAPE_UI_VERTICAL_OFFSET + 1
        drawPoint(pointXCoordinate, pointYCoordinate)
    }

    private fun drawGameOver() {
        terminal.setCursorPosition(
            rightBoundaryXCoordinate + RIGHT_SIDE_UI_HORIZONTAL_OFFSET,
            topBoundaryYCoordinate + GAME_OVER_UI_VERTICAL_OFFSET
        )
        GAME_OVER_LABEL.forEach { char -> terminal.putCharacter(char) }
    }

    private fun putCharacterAtPosition(char: Char, x: Int, y: Int) {
        terminal.setCursorPosition(x, y)
        terminal.putCharacter(char)
    }
}